#pragma once
#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <vector>
#include <utility>
#include <string>
#include<ios>
#include <fstream>
#include <algorithm>
#include <set>
#include "logger.hpp"


/**
 * Struct representing vertex of graph. Stores two values - vertex color and vertex_degree.
 */
struct Vertex {
    explicit Vertex(int vertexDegree, int color, int id);

    int color;
    int vertex_degree;
    int id;
};

/**
 * Class for representing graph. Contains basic methods for graph and saves it in private variables.
 *
 * This representation of graph is two std::vectors, one for storing edges, second for storing vertices.
 */
class Graph {
private:
    /**
     * std::vector<std::pair<int, int>> - vector containg pair of integers, ids of vertices which have common edge.
     */
    std::vector<std::pair<int, int>> edges;
    /**
     *  std::vector<Vertex>, used for storing vertices
     */
    std::vector<Vertex> vertices;
    /**
     * Integer graph size - number of total vertices.
     */
    int graph_size = 0;

    /**
     * Maximal degree of vertex in graph.
     */
    int max_degree = -1;

public:

    /**
     * Class constructor, creates empty graph. Vertices and edges should be added with add_vertex() and add_edge() functions.
     */
    Graph();

    /**
     * Class destructor.
     */
    ~Graph();

    /**
     * Adds vertex to graph. Accepts number of edges emanating from it = vertex degree.
     *
     * @param number_of_edges int, value of vertex degree
     */
    void add_vertex(int& number_of_edges);

    /**
     * Adds edge to graph. Parameters represent ids of vertices between which the edge is.
     *
     * @param first_edge int, id of first vertex
     * @param second_edge int, id of second vertex
     */
    void add_edge(int first_edge, int second_edge);

    /**
     * Getter for getting graph edges.
     *
     * @return std::vector<std::pair<int, int>> containing graph edges.
     */
    const std::vector<std::pair<int, int>> &get_edges() const;

    /**
     * Getter for getting graph vertices.
     *
     * @return std::vector<Vertex> containing graph vertices.
     */
    const std::vector<Vertex> &get_vertices() const;

    /**
    * Returns graph size.
    *
    * @return int, size of graph/number of its vertices
    */
    int get_number_of_vertices() const;

    /**
     * Sets vertex placed on given index given color.
     *
     * @param vertex_index int, index of vertex which should be colored
     * @param color int representing color which should be used for coloring vertex
     */
    void set_vertex_color(int vertex_index, int color);

    /**
     * Finds and returns neighbours of vertex on given id.
     *
     * @param vertex int, id of vertex
     * @return std::set<int> containing ids of vertices which have common edge with desired vertex
     */
    std::set<int> get_vertex_neighbours(int& vertex);

    /**
     * Logs colors of vertices on level INFO.
     */
    void log_vertices_colors() const;
};

/**
 * Parses stream in valid format to graph.
 *
 * @param stream input stream from which should be graph constructed
 * @return Graph constructed from stream
 */
Graph parse_graph(std::ifstream& stream);

/**
 * In std::vector of edges (std::pair<int, int>) checks if edge (also std::pair<int, int>) is existing.
 *
 * @param edges std::vector<std::pair<int, int>> of edges existing in graph
 * @param edge std::pair<int, int> representing egde that should be checked for existence
 * @return true if edge is existing, else false
 */
bool is_edge_existing(std::vector<std::pair<int, int>> edges, std::pair<int, int> edge);

/**
 * Function for parsing two init rows with count of edges and vertices.
 *
 * @param row std::string, row which should be parsed
 * @return Parsed int, number of edges/vertices
 */
int parse_count(const std::string& row);

/**
 * Checks validity of edge - no self loops. Also eliminates possibility that edge is created twice. If check is OK, edge is created and added to graph.
 *
 * @param graph reference to instance of class Graph
 * @param to id of second vertex in edge
 * @param vertex_counter reference to all vertices counter, also used as value for vertex id
 * @param edges_counter reference to all edges counter
 */
void check_and_add_edge(Graph& graph, int& to, int& vertex_counter, int& edges_counter);

/**
 * Takes row of values for creating vertex and its edges, parses it and checks validity of input.
 *
 * @param graph reference to instance of class Graph
 * @param row reference to row from stream
 * @param vertices_counter reference to all vertices counter
 * @param edges_counter reference to all edges counter
 */
void parse_edges_and_add(Graph& graph, const std::string& row, int& vertices_counter, int& edges_counter);


#endif