cmake_minimum_required(VERSION 3.5)
project(pcc-semestralni-prace LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_executable(pcc-semestralni-prace
        graph.hpp
        graph.cpp
        logger.cpp
        logger.hpp
        algorithms/RLF.hpp
        algorithms/RLF.cpp
        algorithms/GreedyColoring.hpp
        algorithms/GreedyColoring.cpp
        algorithms/DSatur.hpp
        algorithms/DSatur.cpp
        main.cpp
)


if ( CMAKE_CXX_COMPILER_ID MATCHES "Clang|AppleClang|GNU" )
    target_compile_options( pcc-semestralni-prace PRIVATE -Wall -Wextra -Wunreachable-code -Wpedantic)
endif()
if ( CMAKE_CXX_COMPILER_ID MATCHES "MSVC" )
    target_compile_options( pcc-semestralni-prace PRIVATE /W4 )
endif()

