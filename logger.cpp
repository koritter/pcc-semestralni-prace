#pragma once

#include "logger.hpp"

#include<string>
#include<ios>
#include <ctime>
#include <iomanip>
#include <chrono>
#include <iostream>

Logger::Logger(const std::string& log_directory) {
    std::ostringstream log_path;
    log_path << log_directory << "/log_" << get_timestamp() << ".txt";

    file_for_log.open(log_path.str(), std::ios::app);

    if (!file_for_log.is_open()) {
        throw std::runtime_error("Log file cannot be opened.");
    }
}

std::string Logger::get_timestamp() {
    auto currentTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::ostringstream oss;
    oss << std::put_time(std::localtime(&currentTime), "%Y-%m-%d %H:%M:%S");
    return oss.str();
}

std::string Logger::get_level_string(LogLevel& level) {
    switch (level) {
        case DEBUG:
            return "DEBUG";
            break;
        case INFO:
            return "INFO";
            break;
        case ERROR:
            return "ERROR";
            break;
        case WARNING:
            return "WARNING";
            break;
    }

}

void Logger::log(LogLevel level, const std::string& message) {

    if (level >= logging_level) {
        std::string timestamp = get_timestamp();

        // Logging to console
        if (console_log) {
            std::cout << "[" << timestamp << "] [" << get_level_string(level) << "] " << message << std::endl;
        }

        // Logging to file
        if (file_log and file_for_log.is_open()) {
            file_for_log << "[" << timestamp << "] [" << get_level_string(level) << "] " << message << std::endl;
        }
    }
}

void Logger::set_console_log(bool new_console_log) {
    console_log = new_console_log;
}

void Logger::set_file_log(bool new_file_log) {
    file_log = new_file_log;
}

Logger::~Logger() {
    if (file_for_log.is_open()) {
        file_for_log.close();
    }
}

void Logger::set_logging_level(LogLevel new_logging_level) {
    logging_level = new_logging_level;
}

Logger logger("../logs");





