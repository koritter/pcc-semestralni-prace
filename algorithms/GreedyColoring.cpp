#include "GreedyColoring.hpp"

#include <set>

std::set<int> GreedyColoring::get_neighbours_colors(Graph& g, int& vertex) {
    std::set<int> colors;
    for (auto& v : g.get_vertex_neighbours(vertex)) {
        colors.insert(g.get_vertices().at(v).color);
    }
    colors.erase(-1);
    return colors;
}

int GreedyColoring::find_first_available_color(std::set<int>& neighbour_colors) {
    for (int i = 0; i < static_cast<int>(neighbour_colors.size())+1; ++i) {
        if (neighbour_colors.count(i) < 1) {
            return i;
        }
    }
    return -1;
}

void GreedyColoring::color_graph(Graph& g) {
    for (int i = 0; i < static_cast<int>(g.get_vertices().size()); ++i) {
        std::set<int> used_colors = get_neighbours_colors(g, i);
        g.set_vertex_color(i, find_first_available_color(used_colors));
    }
}
