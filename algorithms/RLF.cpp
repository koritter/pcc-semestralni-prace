#include "RLF.hpp"
#include <set>


int RLF::max_degree_vertex(std::set<int>& vertices, Graph& g) {
    int max_deg = -1, max_ver = -1;

    for (auto& v : vertices) {
        if(g.get_vertices().at(v).vertex_degree > max_deg) {
            max_deg = g.get_vertices().at(v).vertex_degree;
            max_ver = v;
        }
    }

    return max_ver;
}

void RLF::find_and_exclude_neighbours(std::set<int>& excluded, Graph& g, int& vertex, std::set<int>& unused_set) {
    for (auto& edge : g.get_edges()) {
        if (edge.first == vertex and g.get_vertices().at(edge.second).color == -1) {
            excluded.insert(edge.second);
            unused_set.erase(edge.second);
        } else if (edge.second == vertex and g.get_vertices().at(edge.first).color == -1) {
            excluded.insert(edge.first);
            unused_set.erase(edge.first);
        }
    }
}

int RLF::find_vertex_with_max_excluded_neighbours(std::set<int>& vertices_to_search, std::set<int>& excluded_vertices,  Graph& g) {
    int max_deg = -1, max_ver = -1;

    for (auto& v: vertices_to_search) {
        int cnt = 0;
        for (auto &edge: g.get_edges()) {
            if ((excluded_vertices.count(edge.first) > 0 and edge.second == v) or (excluded_vertices.count(edge.second) > 0 and edge.first == v)) {
                ++cnt;
            }
        }
        if (cnt > max_deg) {
            max_deg = cnt;
            max_ver = v;
        }
    }

    return max_ver;
}

bool RLF::find_and_color_maximal_independent_set(std::set<int>& unused_set, Graph& g, int color) {
    if (unused_set.empty()) {
        return true;
    } else {

        // init new sets for independent set and excluded vertices
        std::set<int> independent_set;
        std::set<int> excluded_set;

        // find vertex with maximal degree and add it independent set
        int max_ver = max_degree_vertex(unused_set, g);
        independent_set.insert(max_ver);
        unused_set.erase(max_ver);

        // exclude its neighbors
        find_and_exclude_neighbours(excluded_set, g, max_ver, unused_set);

        // until you used (excluded or included) or neighbours, do:
        while (!unused_set.empty()) {
            // find vertex with most excluded neighbors and add it to independent set
            max_ver = find_vertex_with_max_excluded_neighbours(unused_set, excluded_set, g);
            independent_set.insert(max_ver);
            unused_set.erase(max_ver);

            // exclude its neighbors
            find_and_exclude_neighbours(excluded_set, g, max_ver, unused_set);
        }

        // color independent set
        color_set(independent_set, g, color);

        // call this function again on excluded set of vertices
        return find_and_color_maximal_independent_set(excluded_set, g, color + 1);
    }

}

void RLF::color_set(std::set<int>& set, Graph& g, int color) {
    for (auto& v : set) {
        g.set_vertex_color(v, color);
    }
}

void  RLF::color_graph(Graph& g) {
    std::set<int> unused_set;
    for (int i = 0; i < static_cast<int>(g.get_vertices().size()); ++i) {
        unused_set.insert(i);
    }
    find_and_color_maximal_independent_set(unused_set, g, 0);
}
