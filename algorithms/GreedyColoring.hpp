

#ifndef PCC_SEMESTRALNI_PRACE_GREEDYCOLORING_HPP
#define PCC_SEMESTRALNI_PRACE_GREEDYCOLORING_HPP

#include <vector>
#include <utility>
#include <string>
#include<ios>
#include <fstream>
#include <algorithm>
#include <set>
#include "../graph.hpp"

/**
 * Class containing functions for greedy coloring algorithm.
 * Source for algorithm logic: https://en.wikipedia.org/wiki/Greedy_coloring
 */
class GreedyColoring {
private:

    /**
     * Finds every neighbour to given vertex and its color. Return set of colors used by vertex neighbours.
     *
     * @param g reference to instance of class Graph
     * @param vertex int representing vertex index under which it is stored in the graph
     * @return std::set<int> representing colors used by vertex neigbours
     */
    static std::set<int> get_neighbours_colors(Graph& g, int& vertex);

    /**
     * Finds lowest available color for vertex. Accepts a list of colors used by vertex neighbors and choose the smallest unused color.
     *
     * @param neighbour_colors std::set<int> representing colors which are used by vertex neighbours
     * @return int, representing available color for vertex.
     */
    static int find_first_available_color(std::set<int>& neighbour_colors);
public:

    /**
     * Main function of class. Colors graph using greedy coloring algorithm.
     *
     * @param g reference to instance of class Graph
     */
    static void color_graph(Graph& g);
};


#endif //PCC_SEMESTRALNI_PRACE_GREEDYCOLORING_HPP
