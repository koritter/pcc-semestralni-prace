#ifndef PCC_SEMESTRALNI_PRACE_RLF_HPP
#define PCC_SEMESTRALNI_PRACE_RLF_HPP

#include <vector>
#include <utility>
#include <string>
#include <ios>
#include <fstream>
#include <algorithm>
#include <set>
#include "../graph.hpp"

/**
 * Class containing functions for recursive largest first algorithm.
 * Source for algorithm logic: https://en.wikipedia.org/wiki/Recursive_largest_first_algorithm
 */
class RLF {
private:
    /**
     * Searches vertex with highest degree among the others in given set of vertices.
     *
     * @param vertices std::set<int> representing ids of vertices in graph
     * @param g reference to instance of class Graph
     * @return int representing id of vertex with highest degree form vertices
     */
    static int max_degree_vertex(std::set<int>& vertices, Graph& g);

    /**
     * Finds neighbours of vertex. Erases ids of these neighbours from unused_set and saves them to excluded set.
     *
     * @param excluded reference to std::set<int> representing excluded vertices from main set
     * @param g reference to instance of class Graph
     * @param vertex int, id of vertex
     * @param unused_set reference to std::set<int> representing vertices which haven't been used in code
     */
    static void find_and_exclude_neighbours(std::set<int>& excluded, Graph& g, int& vertex, std::set<int>& unused_set);

    /**
     * Searches in vertices_to_search vertex with most excluded neighbours (saved in excluded_vertices). Return id of vertex with maximal value.
     *
     * @param vertices_to_search reference to std::set<int> representing vertices which haven't been used in code
     * @param excluded_vertices reference to std::set<int> representing excluded vertices from main set
     * @param g reference to instance of class Graph
     * @return int, id of vertex with highest excluded neighbours.
     */
    static int find_vertex_with_max_excluded_neighbours(std::set<int>& vertices_to_search, std::set<int>& excluded_vertices,  Graph& g);

    /**
     * Recursive function. Starts independent set with vertex with maximal degree among the others in unused_set. Than adds vertices which are noc directly connected to any member of independent set.
     * In the end calls itself to set of vertices excluded from independent set. If unused set is empty, function returns true.
     *
     * @param unused_set std::set<int> representing vertices of graph which need to be colored
     * @param g reference to instance of class Graph
     * @param color int, representing color which should be used in current run for independent set of vertices
     * @return True if every vertex in graph has been colored.
     */
    static bool find_and_color_maximal_independent_set(std::set<int>& unused_set, Graph& g, int color);

    /**
     * Accepts set of vertices and colors them the same (given) color.
     *
     * @param set std::set<int> of ids of vertices which should be colored
     * @param g reference to instance of class Graph
     * @param color int, color which should be used for coloring
     */
    static void color_set(std::set<int>& set, Graph& g, int color);

public:
    /**
     * Main function of class. Colors graph using recursive largest first algorithm.
     *
     * @param g reference to instance of class Graph
     */
    static void color_graph(Graph& g);

};

#endif //PCC_SEMESTRALNI_PRACE_RLF_HPP
