#ifndef PCC_SEMESTRALNI_PRACE_DSATUR_HPP
#define PCC_SEMESTRALNI_PRACE_DSATUR_HPP

#include <vector>
#include <utility>
#include <string>
#include<ios>
#include <fstream>
#include <algorithm>
#include <set>
#include "../graph.hpp"

/**
 * Class containing functions for DSatur algorithm.
 * Source for algorithm logic: https://en.wikipedia.org/wiki/DSatur
 */
class DSatur {
private:
    /**
     * Finds vector with maximal saturation. Saturation = number of colors used by vertex neighbours.
     * In case of tie chooses vertex with highest degree in uncolored subgraph to g.
     * Returns id of founded vertex.
     *
     * @param g reference to instance of class Graph
     * @param saturation vector (size = number of vertices), each value representing saturation of corresponding vertex.
     * @return int, id of vertex with highest saturation
     */
    static int get_max_saturation_vertex(Graph& g, std::vector<int>& saturation);

    /**
     * Fills set of used colors with colors used by vertex neighbors.
     *
     * @param g reference to instance of class Graph
     * @param vertex int, id of vertex for which we want search
     * @param used_colors reference to set of used colors
     */
    static void get_used_colors(Graph& g, int vertex, std::set<int>& used_colors);

    /**
     * Updates saturation vector for neighbours of vertex.
     *
     * @param g reference to instance of class Graph
     * @param vertex int, id of newly colored vertex
     * @param saturation vector (size = number of vertices), each value representing saturation of corresponding vertex.
     */
    static void update_saturation(Graph& g, int vertex, std::vector<int>& saturation);

    /**
     * Finds lowest available color for vertex. Accepts a list of colors used by vertex neighbors and choose the smallest unused color.
     *
     * @param g g reference to instance of class Graph
     * @param vertex id of vertex for which we want search
     * @param used_colors reference to set of used colors
     * @return int, representing available color for vertex.
     */
    static int get_color(Graph& g, int& vertex, std::set<int>& used_colors);


    /**
    * Function for getting degree of vertex in uncolored subgraph. Counts number of uncolored neighbours.
    *
    * @param g reference to instance of class Graph
    * @param vertex int representing vertex index for which we're searching subgraph
    * @return int, degree of vertex in uncolored subgraph
    */
    static int get_largest_degree_uncolored_subgraph(Graph& g, int& vertex);

public:
    /**
     * Main function of class. Colors graph using DSatur algorithm.
     *
     * @param g reference to instance of class Graph
     */
    static void color_graph(Graph& g);
};

#endif //PCC_SEMESTRALNI_PRACE_DSATUR_HPP
