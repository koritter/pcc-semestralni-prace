#include "DSatur.hpp"
#include <set>

int DSatur::get_largest_degree_uncolored_subgraph(Graph& g, int& vertex) {
    int cnt = 0;
    for (auto& neighbour: g.get_vertex_neighbours(vertex)) {
        if(g.get_vertices().at(neighbour).color == -1) {
            ++cnt;
        }
    }
    return cnt;
}

int DSatur::get_max_saturation_vertex(Graph& g, std::vector<int>& saturation) {
    int max_saturation = -1;
    int max_saturation_vertex = -1;

    for (int i = 0; i < g.get_number_of_vertices(); ++i) {
        if (g.get_vertices().at(i).color == -1 && saturation[i] > max_saturation) { // saturation of vertex is higher than current value
            max_saturation = saturation[i];
            max_saturation_vertex = i;
        } else if (g.get_vertices().at(i).color == -1 && saturation[i] == max_saturation) { // case of tie
            if (get_largest_degree_uncolored_subgraph(g, i) > get_largest_degree_uncolored_subgraph(g, max_saturation_vertex)) {
                max_saturation_vertex = i;
            }
        }
    }

    return max_saturation_vertex;
}

void DSatur::get_used_colors(Graph& g, int vertex, std::set<int>& used_colors) {
    used_colors.clear();

    for (auto& v : g.get_vertex_neighbours(vertex)) {
        used_colors.insert(g.get_vertices().at(v).color);
    }

    used_colors.erase(-1);
}

int DSatur::get_color(Graph& g, int& vertex, std::set<int>& used_colors) {
    get_used_colors(g, vertex, used_colors);

    for (int i = 0; i < static_cast<int>(used_colors.size()) + 1; ++i) {
        if (used_colors.count(i) < 1) {
            return i;
        }
    }

    return -1;
}

void DSatur::update_saturation(Graph& g, int vertex, std::vector<int>& saturation) {
    for (const auto& edge : g.get_edges()) {
        if (edge.first == vertex && g.get_vertices().at(edge.second).color == -1) {
            ++saturation[edge.second];
        } else if (edge.second == vertex && g.get_vertices().at(edge.first).color == -1) {
            ++saturation[edge.first];
        }
    }
}

void DSatur::color_graph(Graph& g) {
    std::vector<int> saturation(g.get_number_of_vertices(), 0);
    std::set<int> usedColors;

    // For every vertex in graph perform following steps - find most saturated uncolored vertex, choose its color, update saturation
    for (int i = 0; i < g.get_number_of_vertices(); ++i) {
        int vertex = get_max_saturation_vertex(g, saturation);
        g.set_vertex_color(vertex, get_color(g, vertex, usedColors));
        update_saturation(g, vertex, saturation);
    }
}
