
#include <iostream>
#include <chrono>
#include "algorithms/RLF.hpp"
#include "algorithms/GreedyColoring.hpp"
#include "algorithms/DSatur.hpp"
#include "graph.hpp"

template <typename Duration>
long long to_ms(const Duration& d) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(d).count();
}

/**
 * Runs the greedy coloring algorithm on provided graph.
 *
 * @param g reference to instance of class Graph
 */
void color_greedy_coloring(Graph& g) {
    logger.log(INFO, "Greedy coloring algorithm started.\n");
    auto start = std::chrono::high_resolution_clock::now();
    GreedyColoring::color_graph(g);
    auto end = std::chrono::high_resolution_clock::now();

    auto duration_ms = to_ms(end - start);
    logger.log(INFO, "Greedy coloring needed " + std::to_string(duration_ms) + " ms to finish.\n");
    g.log_vertices_colors();

}

/**
 * Runs the DSatur algorithm on provided graph.
 *
 * @param g reference to instance of class Graph
 */
void color_DSatur(Graph& g) {
    logger.log(INFO, "DSatur algorithm started.\n");
    auto start = std::chrono::high_resolution_clock::now();
    DSatur::color_graph(g);
    auto end = std::chrono::high_resolution_clock::now();

    auto duration_ms = to_ms(end - start);
    logger.log(INFO, "DSatur needed " + std::to_string(duration_ms) + " ms to finish.\n");
    g.log_vertices_colors();
}

/**
 * Runs the recursive largest first algorithm on provided graph.
 *
 * @param g reference to instance of class Graph
 */
void color_RLF(Graph& g) {
    logger.log(INFO, "Recursive largest first algorithm started.\n");
    auto start = std::chrono::high_resolution_clock::now();
    RLF::color_graph(g);
    auto end = std::chrono::high_resolution_clock::now();

    auto duration_ms = to_ms(end - start);
    logger.log(INFO, "Recursive largest first needed " + std::to_string(duration_ms) + " ms to finish.\n");
    g.log_vertices_colors();
}

/**
 * Translate logging level given by user so it could be stored in Logger class.
 *  0: DEBUG
 *  1: INFO
 *  2: WARNING
 *  3: ERROR
 *
 * @param logging_level int representing logging level.
 * @return LogLevel representation of logging level
 */
LogLevel translate_logging_level(int& logging_level) {
    switch (logging_level) {
        case 0:
            return LogLevel::DEBUG;
            break;
        case 1:
            return LogLevel::INFO;
            break;
        case 2:
            return LogLevel::WARNING;
            break;
        case 3:
            return LogLevel::ERROR;
            break;
        default:
            return LogLevel::INFO;
            break;
    }
}

/**
 * Set logger setting for current run of program.
 *
 * @param logging_level int, level of logging
 * @param console int, console logging on/off
 * @param file int, file logging on/off
 */
void logger_settings(int& logging_level, int& console, int& file) {
    logger.set_logging_level(translate_logging_level(logging_level));

    if (console == 1 or console == 0) {
        logger.set_console_log(console);
    } else {
        throw std::runtime_error("Invalid input for setting logger.");
    }
    if (file == 1 or file == 0) {
        logger.set_file_log(file);
    } else {
        throw std::runtime_error("Invalid input for setting logger.");
    }
}

/**
 * Opens file with graph representation, parses it.
 *
 * @param graph_file std::string name of file with graph
 * @return Graph parsed from file.
 */
Graph parse_graph_from_file(std::string& graph_file) {
    std::ifstream file("../graphs/" + graph_file);

    if (!file.is_open()) {
        logger.log(ERROR, "Graph file cannot be opened.");
        throw std::runtime_error("Graph file cannot be opened.");
    }

    logger.log(INFO, "Parsing file " + graph_file);
    return parse_graph(file);
}

/**
 * Writes help to console.
 */
void help_writer() {
    std::cout << "Help: Program with switches\n";
    std::cout << "-h, --help:   Display help\n\n";
    std::cout << "-logger:      Set logger for program. Three additional arguments required: <logging-level> <console-on> <file-on>.\n"
              << "              <logging-level> : 0 (DEBUG), 1 (INFO), 2 (WARNING), 3 (ERROR)\n"
              << "              <console-on>: 0 (console logging OFF), 1 (console logging ON)\n"
              << "              <file-on>: 0 (file logging OFF), 1 (file logging ON)\n\n";
    std::cout << "-greedy:      Runs a program with a greedy coloring algorithm. One additional argument required: <graph-file>\n"
              << "              <graph-file>: Name of graph file to be used in program.\n\n";
    std::cout << "-DSatur:      Runs a program with a DSatur algorithm. One additional argument required: <graph-file>\n"
              << "              <graph-file>: Name of graph file to be used in program.\n\n";
    std::cout << "-RLF:         Runs a program with a recursive largest first algorithm. One additional argument required: <graph-file>\n"
              << "              <graph-file>: Name of graph file to be used in program.\n\n";
    std::cout << "-v:           Display program version\n";
}

int main(int argc, char* argv[]) {
    std::string graph_file;


    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];

        if (arg == "-h" || arg == "--help") {
            help_writer();
            break;
        } else if (arg == "-logger") {
            try {
                if (i + 3 < argc) {
                    int logging_level = std::stoi(argv[i + 1]);
                    int console = std::stoi(argv[i + 2]);
                    int file = std::stoi(argv[i + 3]);

                    logger_settings(logging_level, console, file);

                    i += 3;
                } else {
                    throw std::runtime_error("Not enough arguments for -logger.");
                }
            } catch (const std::exception& e) {
                throw std::runtime_error("Invalid input for -logger: " + std::string(e.what()));
            }

        } else if (arg == "-greedy") {
            if (i + 1 < argc) {
                try {
                    graph_file = argv[i + 1];
                    Graph g = parse_graph_from_file(graph_file);
                    color_greedy_coloring(g);
                    ++i;
                } catch (const std::exception& e) {
                    throw std::runtime_error("Error processing the graph file: " + std::string(e.what()));
                }
            } else {
                throw std::invalid_argument("Invalid input for -greedy. Specify the graph file.");
            }

        } else if (arg == "-DSatur") {
            if (i + 1 < argc) {
                try {
                    graph_file = argv[i + 1];
                    Graph g = parse_graph_from_file(graph_file);
                    color_DSatur(g);
                    ++i;
                } catch (const std::exception& e) {
                    throw std::runtime_error("Error processing the graph file: " + std::string(e.what()));
                }
            } else {
                throw std::invalid_argument("Invalid input for -DSatur. Specify the graph file.");
            }

        } else if (arg == "-RLF") {
            if (i + 1 < argc) {
                try {
                    graph_file = argv[i + 1];
                    Graph g = parse_graph_from_file(graph_file);
                    color_RLF(g);
                    ++i;
                } catch (const std::exception& e) {
                    throw std::runtime_error("Error processing the graph file: " + std::string(e.what()));
                }
            } else {
                throw std::invalid_argument("Invalid input for -RLF. Specify the graph file.");
            }

        } else if (arg == "-v") {
            std::cout << "Program version: 1.0\n";
        } else {
            std::cout << "Invalid command: " << arg << "\n";
        }
    }

    return 0;
}