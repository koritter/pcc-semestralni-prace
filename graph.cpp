#include "graph.hpp"
#include <sstream>
#include <algorithm>
#include <iostream>
#include <fstream>


bool is_edge_existing(std::vector<std::pair<int, int>> edges, std::pair<int, int> edge) {
    logger.log(DEBUG, "Function is_edge_existing() was entered. Looking for edge " + std::to_string(edge.first) + "-" + std::to_string(edge.second));
    auto it = std::find(edges.begin(), edges.end(), edge);
    if (it != edges.end()) {
        logger.log(DEBUG, "Edge was found in existing edges, returning true.");
        return true;
    } else {
        logger.log(DEBUG, "Edge was not found in existing edges, returning false.");
        return false;
    }
}

int parse_count(const std::string& row) {
    logger.log(DEBUG, "Function parse_count was entered for row: " + row);
    return std::stoi(row.substr(row.find(':') + 2));
}

void check_and_add_edge(Graph& graph, int& to, int& vertex_counter, int& edges_counter) {
    logger.log(DEBUG, "Function check_and_add_edge() was entered for edge from " + std::to_string(vertex_counter) + " to " + std::to_string(to) + ".");
    if (to == vertex_counter) {
        logger.log(ERROR, "Loop detected: self-loops are not allowed.");
        throw std::invalid_argument("Loop detected: self-loops are not allowed.");
    } else if (!is_edge_existing(graph.get_edges(), std::make_pair(to, vertex_counter))) {
        logger.log(DEBUG, "Adding edge to graph edges.");
        graph.add_edge(vertex_counter, to);
    }
    ++edges_counter;
    logger.log(DEBUG, "Increasing edges_counter. New value:" + std::to_string(edges_counter));
}

void parse_edges_and_add(Graph& graph, const std::string& row, int& vertices_counter, int& edges_counter) {
    logger.log(DEBUG, "Function parse_edges_and_add() was entered for row " + row + ".");

    std::istringstream iss(row.substr(row.find(':') + 2));
    int vertex_edges, vertex_edges_counter = 0;

    iss >> vertex_edges;
    logger.log(INFO, "Count of edges for Vertex " + std::to_string(graph.get_vertices().size() + 1) + " is " + std::to_string(vertex_edges) + ".");

    while (iss >> std::ws, !iss.eof()) {
        int temp;
        iss >> temp;
        --temp;

        check_and_add_edge(graph, temp, vertices_counter, edges_counter);
        ++vertex_edges_counter;
        logger.log(DEBUG, "Increasing vertex_edges_counter. New value:" + std::to_string(vertex_edges_counter));
    }

    if (vertex_edges_counter != vertex_edges) {
        logger.log(ERROR, "Invalid input file format: number of edges for a vertex is not corresponding.");
        throw std::invalid_argument("Invalid input file format: number of edges for a vertex is not corresponding.");
    }

    ++vertices_counter;
    logger.log(DEBUG, "Increasing vertices_counter. New value:" + std::to_string(vertices_counter));

    logger.log(DEBUG, "Adding vertex to graph vertices.");
    graph.add_vertex(vertex_edges);

    logger.log(INFO, "Vertex " + std::to_string(graph.get_vertices().size()) + " was successfully parsed and saved to graph.");
}

Graph parse_graph(std::ifstream& stream) {
    logger.log(INFO, "Function parse_graph() was entered. Stream is being parsed into a graph.");

    Graph graph;
    int vertexes_count, edges_count;
    int edges_counter = 0, vertexes_counter = 0;

    std::string row;

    getline(stream, row);
    vertexes_count = parse_count(row);
    logger.log(INFO, "Parsing number of vertices. Number: " + std::to_string(vertexes_count) + ".");

    getline(stream, row);
    edges_count = parse_count(row);
    logger.log(INFO, "Parsing number of edges. Number: " + std::to_string(edges_count) + ".");

    while (getline(stream, row)) {
        logger.log(DEBUG, "Parsing vertex row: " + row);
        parse_edges_and_add(graph, row, vertexes_counter, edges_counter);
    }

    if (vertexes_counter != vertexes_count) {
        logger.log(ERROR, "Invalid input file format: number of vertices is not corresponding.");
        throw std::invalid_argument("Invalid input file format: number of vertices is not corresponding.");
    }

    if (edges_counter / 2 != edges_count) {
        logger.log(ERROR, "Invalid input file format: number of edges is not corresponding.");
        throw std::invalid_argument("Invalid input file format: number of edges is not corresponding.");
    }

    logger.log(INFO, "Stream was successfully parsed to graph.");
    return graph;
}

const std::vector<std::pair<int, int>> &Graph::get_edges() const {
    return edges;
}

const std::vector<Vertex> &Graph::get_vertices() const {
    return vertices;
}

void Graph::add_vertex(int& number_of_edges) {
    Graph::vertices.emplace_back(number_of_edges, -1, Graph::graph_size);
    max_degree = std::max(max_degree, number_of_edges);
    ++Graph::graph_size;
}

void Graph::add_edge(int first_edge, int second_edge) {
    Graph::edges.emplace_back(first_edge, second_edge);
}

Graph::Graph() = default;

Graph::~Graph() = default;

int Graph::get_number_of_vertices() const {
    return Graph::graph_size;
}

void Graph::set_vertex_color(int vertex_index, int color) {
    vertices[vertex_index].color = color;
}

std::set<int> Graph::get_vertex_neighbours(int& vertex) {
    std::set<int> neighbours;

    for (auto& edge : edges) {
        if (edge.first == vertex) {
            neighbours.insert(edge.second);
        } else if (edge.second == vertex) {
            neighbours.insert(edge.first);
        }
    }

    return neighbours;
}

void Graph::log_vertices_colors() const {
    for (const Vertex& vertex : vertices) {
        logger.log(INFO, "Vertex " + std::to_string(vertex.id + 1) + " has color " + std::to_string(vertex.color) + ".");
    }
}

Vertex::Vertex(int vertexDegree, int color, int id) : color(color), vertex_degree(vertexDegree), id(id) {}

