import random

def generate_graph():
    # Step 1: Determine the number of vertices and edges
    num_vertices = 993  # Example range from 5 to 10 vertices
    max_edges = num_vertices * (num_vertices - 1) // 2  # Maximum possible edges
    num_edges = random.randint(num_vertices - 1, max_edges)  # At least a tree structure, at most fully connected

    # Step 2: Initialize vertices with no neighbors
    graph = {i: set() for i in range(1, num_vertices + 1)}

    # Step 3: Randomly assign neighbors ensuring the graph rules
    edges = 0
    while edges < num_edges:
        v1, v2 = random.sample(graph.keys(), 2)
        if v2 not in graph[v1]:
            graph[v1].add(v2)
            graph[v2].add(v1)
            edges += 1

    # Step 4: Convert sets to sorted lists for consistent output
    for vertex in graph:
        graph[vertex] = sorted(list(graph[vertex]))

    return num_vertices, num_edges, graph

def format_graph_output(num_vertices, num_edges, graph):
    output = f"Number of vertices: {num_vertices}\nNumber of edges: {num_edges}\n"
    for vertex, neighbors in graph.items():
        output += f"Vertex {vertex}: {len(neighbors)} {' '.join(map(str, neighbors))}\n"
    return output

# Generate and print the graph
num_vertices, num_edges, graph = generate_graph()
print(format_graph_output(num_vertices, num_edges, graph))