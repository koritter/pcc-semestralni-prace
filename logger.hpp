#ifndef LOGGER_HPP
#define LOGGER_HPP

#pragma once

#include<string>
#include<ios>
#include <fstream>


/**
 * Enum of logging levels.
 */
enum LogLevel {
    DEBUG,
    INFO,
    WARNING,
    ERROR
};

/**
 * Class for logger for this code. Contains four levels of logging, DEBUG, INFO, WARNING, ERROR saved in enum LogLevel.
 */
class Logger {
private:
    /**
     * Output stream for logging to log file.
     */
    std::ofstream file_for_log;

    /**
     * Variable to turn console logging on and off.
     */
    bool console_log = false;

    /**
     * Variable to turn file logging on and off
     */
    bool file_log = true;

    /**
     * Variable of logging level which should be written to console/file (if these are switched on)
     */
    LogLevel logging_level = INFO;

    /**
     * Helper method for getting the timestamp in a readable text format.
     *
     * @return std::string with formatted timestamp
     */
    static std::string get_timestamp();

    /**
     * Helper method for getting string value of given logging level.
     *
     * @param level LogLevel, level we want to translate into string
     * @return std::string with logging level
     */
    static std::string get_level_string(LogLevel& level);

public:
    /**
     * Logger constructor. Accepts directory where should be log file created. Creates file (with current timestamp) and opens it.
     * After creating logger must be set logging on (for console and file separately) and also set logging level.
     *
     * @param log_directory directory for storing log files
     */
    explicit Logger(const std::string& log_directory);

    /**
     * Logger destructor. Closes log file.
     */
    virtual ~Logger();

    /**
     * Writes line to log - console and file according to current settings of logger.
     *
     * @param level LogLevel, level of log message
     * @param message std::string, message to log
     */
    void log(LogLevel level, const std::string& message);

    /**
     * Setter for on/off state of console logging.
     * False - off, true - on.
     *
     * @param new_console_log bool (true = console logging on, false = console logging off)
     */
    void set_console_log(bool new_console_log);

    /**
     * Setter for on/off state of file logging.
     * False - off, true - on.
     *
     * @param new_file_log bool (true = file logging on, false = file logging off)
     */
    void set_file_log(bool new_file_log);

    /**
     * Setter for logging level. Messages with this level (and higher) would be written in on-logs.
     *
     * @param new_logging_level LogLevel, new level of logging
     */
    void set_logging_level(LogLevel new_logging_level);
};


extern Logger logger;

#endif
